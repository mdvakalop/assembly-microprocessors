INCLUDE M.TXT

DATA_SEG SEGMENT
    MSG1 DB "GIVE 3 HEX DIGITS:",'$'
    MSG2 DB 0AH,0DH,"Decimal:   ",'$'   
    TELEIA DB ".",'$' 
    KENI_GRAMMI DB 0AH,0DH,'$'
DATA_SEG ENDS

CODE_SEG SEGMENT
    ASSUME CS:CODE_SEG, DS:DATA_SEG SS:STACK_SEG
                                      
                                           
MAIN PROC FAR
    MOV AX,DATA_SEG
    MOV DS,AX         
BEGIN:
    PRINT_STR MSG1
    CALL HEX_KEYB           ;DIABAZO AP T KEYBOARD ENA HEX K TO EPISTREFO S BINARY MESO T AL 
    MOV DL,AL               ;APOTHIKEVO TN PROTO ARITMO STO DL
    CALL PRINT_HEX          ;TIPONEI TON HEX ARITHMO
    CALL HEX_KEYB           ;DIABAZO DEFTERO ARITHMO
    MOV DH,AL               ;TON APOTHIKEVO STON DH
    
    PUSH DX                 ;SPROXNO STI STOIBA TUS PROTUS DIO ARITHMUS
    MOV DL,AL               ;GIA NA MPORO NA TIPOSO TN DEFTERO
    CALL PRINT_HEX
    POP DX   
    
    CALL HEX_KEYB           ; DIABAZO TN TRITO ARITHMO
    PRINT_STR TELEIA        ;TIPONO TN TELEIA
    PUSH DX                                        
    MOV DL,AL               ; TIPONO TN TRITO HEX
    CALL PRINT_HEX
    POP DX
               
     ;JMP TELOS 
     ;MOV DL,07H
    ;PRINT_STR TELEIA
    ;CALL PRINT_HEX
    CMP DL,0BH           ;EDO KANO TN ELEGXO AN OI TREIS ARITHMPI ISUNT ME TO B15 OPU EINAI
                          ; I OMADA MAS KAI ARA STAMATAME TO PROGRAMMA
    JNE COOL            ;AN DEN EINAI ISA SIMENI DN EDOSE ARITHMUS G T TELOS OPOTE OLA COOL
    CMP DH,01H
    JNE COOL
    CMP AL,05H
    JNE COOL
    JMP TELOS

COOL:       
    PRINT_STR MSG2      ;SIMIOSI STUS KATAXORITES TR OI ARITHMOI EINAI BINARY  MORFI
    MOV CL,04H          ;EPEIDI THA KANO 4 OLISTHISIS ARISTERA
    ROL DL,CL           ; OLISTHISI ARISTERA 4THESIS=EPI 2^4=EPI 16
    ADD DL,DH           ; STO DL TR BRISKETE TO APOTELESMA OXI DEKADIKO S DIADIKI MORFI   
    PUSH AX             ;GIA NA MI XASO T KLASMATIKO MEROS
    MOV DH,00H          ;KSERO OTI DN THELEI PIO POLLA APO 8BITS ARA MIDENIZO T DH G NA EXO
                        ; TN KATHARO  ARITHMO STN DX
    MOV AX,DX   
    CMP AX,63H               ;BLEPO AN EINAIMEGALITEROS T 99 AN EINAI OK
    JG BIGGER99               ;AN OXI TIPONI ENA EXTRA MIDENIKO SIMFONA  EKFONISI G NA EXO 3 ARITHMS
    PUSH DX
    MOV DL,00H
    PRINT_NUM DL           
    POP DX
    
BIGGER99:
    CALL 16BIN_DEC      ;ALIOS TIPONO TN ARITHMO. ston ax exw ton ari8mo mou (akeraio meros)
    POP AX              ;epanaferw to klasmatiko
    MOV CL,AL           ; METAFERO STN CL,TO KLASMATIKO ARITHMO 
                        ;1/16=.0625
    MOV AX,271H         ;271H=625D
    MOV CH,00H          ;G NA MINI STN CX MONO O ARITHMS CL KATHAROS
    MUL CX              ;TO APOTELESMA T DEKADIKU EINAI STN AX AFU DN KSEPERNA T 16 BITS G NA THELO K DX (271h*fh=249fh)
    MOV CX,AX
    ;MOV DH,00H  
    ;MOV AX,DX  
    ;PRINT_STR MSG2
   ; CALL 16BIN_DEC   
    PRINT_STR TELEIA  
    CMP CX,3E7H             ;AN DN INE MEGALITERO APO XILIA TIPOSE MIDEN
    JA NOZERO               ;ja = jne for unsigned
    MOV DL,00H
    PRINT_NUM DL    
    
NOZERO:
    MOV AX,CX 
    CMP AX,00H
    JE TIPOSE4MIDEN             ;ELEGXOS AN INE ISO M MIDEN APLA TIPONO 4 MIDENIKA APEFTHIAS
   CALL 16BIN_DEC               ;ALIOS TIPONO KANONIKA TN ARITHMO SAN 4 PSIFIA
   CONT:                              ;TIPONO KENI GRAMMI GIA NA PAO STN EPOMENI GRAMMII
    PRINT_STR KENI_GRAMMI              ;KSANA APO TN ARXI MEXRI NA BALO TN HEX B15
    JMP BEGIN
TIPOSE4MIDEN:
PUSH DX
MOV DL,00H
PRINT_NUM DL
PRINT_NUM DL
PRINT_NUM DL
POP DX
JMP CONT    
    
    
    
    
      
    TELOS:
    MOV AX,4C00H                              ;DINO TON ELEGXO PISO STO LITURGIKO
    INT 21H
MAIN ENDP
               
               
               
               
               
               
               
HEX_KEYB PROC NEAR ; DIABAZI HEX MONO KAI TO KANI BINARY,T EPISTREFI STN AL
; p???t??????? ?a? t? ep?st??fe? ?? d?ad??? �?s? t?? AL.

IGNORE:
READ            ; ???�ase t?? ?a?a?t??a ap? t? p???t???????
CMP AL,30H      ; ???tase a? ? ?a?a?t??a? e??a? ??f??
JL IGNORE       ; ?? ??? a????s? t?? ?a? d??�ase ?????
CMP AL,39H
JG ADDR11

SUB AL,30H      ; ??a???? t?? ?a?a??? a???�?? ('0'=30)
JMP ADDR22
ADDR11: 
CMP AL,'A'
JL IGNORE
CMP AL,'F'
JG IGNORE

SUB AL,37H      ; ?etat??p? t?? HEX ASCII se ?a?a??
ADDR22:         ; a???�?('A'=41, 41H-31H=10H)  
RET
HEX_KEYB ENDP               
               
               
               
               
PRINT_HEX PROC NEAR ; PERNI BINARY dl TIPONI HEX KAI EPISTREFI PISO STO BINARY TN ARITHMO STN DL

CMP DL, 9       ; ?? ? a???�?? e??a? �eta?? 0 ?a? 9 p??st??eta?
JG ADDR111      ; ? t?�? 30H ('0'=30H).
ADD DL, 30H  
PRINT DL
SUB DL,30H
JMP ADDR222

ADDR111:
ADD DL, 37H     ; ??af??et??? p??st??eta? ? t?�? 37H ('A'=41H).  
PRINT DL
SUB DL,37H

ADDR222:        ; ??p?se t? ?a?a?t??a st?? ????? 
RET
PRINT_HEX ENDP
               
                       
                       
16BIN_DEC PROC NEAR; ? d?ad???? �??s?eta? st?? ?ata????t? AX       ; PERNI BINARY ax TIPONI DEKADIKO
PUSH CX
MOV CX,0             ; ??d???se t? �et??t? ??f???
ADDR1:
MOV DX,0
MOV BX,10
DIV BX               ; div: ax phliko, dx upoloipo. krataw sth stoiva to upol ki epanalamvanw me phl
PUSH DX             ; ?p????e?se t? ?p????p? st? st??�a
INC CX              ; ????se t? �et??t? ??f???
CMP AX,0            ; ?ta? t? p????? e??a? 0 de? ?p???e? ???? ??f??
JNE ADDR1
ADDR2:              ; ??p?se ta ??f?a ap? t? st??�a (a?t?st??f? se???)
POP DX              ; ???�ase ap? t? st??�a ??a ??f??
PRINT_NUM DL        ; ??p?se t?? a?t?st???? ?a?a?t??a st?? ?????
LOOP ADDR2
POP CX 
RET
16BIN_DEC ENDP                       
               
CODE_SEG ENDS
    END MAIN