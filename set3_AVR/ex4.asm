INCLUDE MMM.TXT          

DATA_SEG SEGMENT
    KENI_GRAMMI DB "      ",0AH,0DH,'$'

CODE_SEG SEGMENT
    ASSUME CS:CODE_SEG, DS:DATA_SEG SS:STACK_SEG
    

MAIN PROC FAR
    MOV CX,0     
    MOV AX,0
    MOV BX,0     
START:
    READ
    CMP AL,'Q'           ;diavazw mexri 3psifious arithmoys
    JE TELOS              ;an patithi Q telos    
    CMP AL,'+'
    JE ADD_NUM1
    CMP AL,'-'
    JE SUBTR_NUM1        ;kataxwritis CX mas leei posa psifia dothikan 
    CMP AL,30H           ;elegxos an dothike egkyro psifio
    JL START
    CMP AL,39H
    JG START
    CMP CX,3             ;lamvanei ypopsin mono ta 3prwta psifia
    JGE START             ;ta ipoloipa ta agnoei
    INC CX                ;perimenontas na dothi + h -
    PUSH AX
    PRINT AL             ;ta prwta 3psifia ta vazw sti stoiva
    JMP START 

ADDITION:
    MOV CX,0     
    MOV AX,0
    MOV BX,0    
    
ADDI:                         ;gia diavasma 2ou arithmoy
    READ
    CMP AL,'Q'
    JE TELOS
    CMP AL,'='                ;otan patithei to = simenei telos diavasmatos 2ou arithmou
    JE ADD_NUM2
    CMP AL,30H                ;elegxos an dothike egkyro psifio
    JL ADDI
    CMP AL,39H
    JG ADDI
    CMP CX,3
    JGE ADDI                  ;kataxwritis CX mas leei posa psifia dothikan
    INC CX                    ;diavase ta 3 prwta psifia mono
    PUSH AX
    PRINT AL
    JMP ADDI 
    
      
ADD_NUM1:
    PRINT '+'               ;an patithei + ypologizw ton 1o arithmo
    POP AX                    ;se diadiko kai to vazw sti stoiva
    MOV AH,0
    SUB AL,30H              ;metatropi se diadiko
    ADD BL,AL                ;BL=BL(0)+monades
    MOV BX,AX
    DEC CX                  ;an eixe dothi mono 1psifio tote telos 1ou arithmou 
    CMP CX,0
    JLE NUM1_IN_STACK
    POP AX                  ;2o psifio gia dekades
    MOV AH,0                 
    SUB AL,30H               ;metatropi se diadiko to 2o psifio
    MOV DX,0AH               ;x10 gia dekades
    MUL DX
    ADD AX,BX                ;BX=BX+dekades=monades+dekades
    MOV BX,AX
    DEC CX
    CMP CX,0                ;an eixan dothi mono 2psifia tote telos 1ou arithmou
    JLE NUM1_IN_STACK
    POP AX
    MOV AH,0
    SUB AL,30H              ;metatropi se diadiko to 3o psifio
    MOV DX,64H              ;x100 gia ekatontades
    MUL DX
    ADD AX,BX               ;BX=BX+ekatontades=monades+dekades+ekatontades
    MOV BX,AX
    DEC CX                   ;telos 3ou psifiou
NUM1_IN_STACK:
    PUSH BX                 ;vale ton 1o arithmo sti stoiva
    CMP CX,0
    JLE ADDI                 ;pigene na diavasis ton 2o
     
          
ADD_NUM2: 
    MOV BX,0                ;midenizw ton kataxwriti BX pou tha mpei o 2os arithmos
    POP AX   
    MOV AH,0                ;idia diadikasi me 1o arithmo
    SUB AL,30H
    ADD BX,AX
    MOV BX,AX
    DEC CX
    CMP CX,0
    JLE PRINT_RES_ADD      ;an einai to telos toy arithmou pigene na ypologiseis
    POP AX                   ;hex kai dec kai na typoseis
    MOV AH,0
    SUB AL,30H
    MOV DX,0AH
    MUL DX
    ADD AX,BX
    MOV BX,AX
    DEC CX
    CMP CX,0
    JLE PRINT_RES_ADD
    POP AX
    MOV AH,0
    SUB AL,30H
    MOV DX,64H
    MUL DX
    ADD AX,BX
    MOV BX,AX
    DEC CX
    CMP CX,0
    JLE PRINT_RES_ADD               
       

PRINT_RES_ADD:
    PUSH AX                 ;2os arithmos sti stoiva epeidh AX epireazetai apo to PRINT
    PRINT '='
    POP AX
    POP BX                  ;vgazw tous 2arithmoys apo ti stoiva 
    ADD AX,BX               ;tous prosthetw
    MOV BX,AX
    PUSH BX                 ;vazw to athroisma sti stoiva
               
               
    ;epeidh i morfi pou ziteitai gia typoma tou hex den exei mprosta 0
    ;den prepei na typwnontai ta 0 mprosta
    ;etsi elegxw kai agnow ola ta psifia mexri na vrw !=0           
PRINT_HEX_ADD:             
    MOV CX,4
    ROL BX,1
    ROL BX,1
    ROL BX,1
    ROL BX,1
    MOV DX,BX
    AND DX,000FH
    CMP DX,0
    JE P3 
    JMP HEX_P
    
P3:
    MOV CX,3
    ROL BX,1
    ROL BX,1
    ROL BX,1
    ROL BX,1
    MOV DX,BX
    AND DX,000FH
    CMP DX,0
    JE P2 
    JMP HEX_P
    
P2:
    MOV CX,2
    ROL BX,1
    ROL BX,1
    ROL BX,1
    ROL BX,1
    MOV DX,BX
    AND DX,000FH
    CMP DX,0
    JE P1 
    JMP HEX_P
    
P1:
    MOV CX,1
    JMP HEX_PRI    
        
    
HEX_P:
    ROR BX,1
    ROR BX,1
    ROR BX,1
    ROR BX,1
HEX_PRI:
    ROL BX,1
    ROL BX,1
    ROL BX,1
    ROL BX,1
    MOV DX,BX
    AND DX,000FH
    CALL PRINT_HEX              ;kaleitai i PRINT_HEX k typonei se hex
    LOOP HEX_PRI                ;epanalamvanetai gia ta psifia meta ta midenika
    
        
    POP BX                      ;typoma se dekadiki morfi
    MOV AX,BX          
    MOV CX,0
PRINT_DEC_ADD:
    MOV DX,0
    MOV BX,10
    DIV BX
    PUSH DX
    INC CX
    CMP AX,0
    JNZ PRINT_DEC_ADD
    PRINT '='
    MOV BX,CX
    
    
ADDR3:
    POP DX
    ADD DX,30H
    PRINT DL
    LOOP ADDR3
    JMP TELOS                  
 ;telos typomatos athroismatos
 
    
            


SUBTR:                       ;idio me pio panw alla gia afairesi
    READ
    CMP AL,'Q'
    JE TELOS
    CMP AL,'='
    JE SUBTR_NUM2
    CMP AL,30H           ;elegxos an dothike egkyro psifio
    JL SUBTR
    CMP AL,39H
    JG SUBTR
    CMP CX,3
    JGE SUBTR
    INC CX
    PUSH AX
    PRINT AL
    JMP SUBTR 
    
      
SUBTR_NUM1:
    PRINT '-'
    POP AX   
    MOV AH,0
    SUB AL,30H
    ADD BL,AL
    MOV BX,AX
    DEC CX
    CMP CX,0
    JLE NUM1_IN_STACK_SUB
    POP AX
    MOV AH,0
    SUB AL,30H
    MOV DX,0AH
    MUL DX
    ADD AX,BX
    MOV BX,AX
    DEC CX
    CMP CX,0
    JLE NUM1_IN_STACK_SUB
    POP AX
    MOV AH,0
    SUB AL,30H
    MOV DX,64H
    MUL DX
    ADD AX,BX
    MOV BX,AX
    DEC CX   
NUM1_IN_STACK_SUB:
    PUSH BX
    CMP CX,0
    JLE SUBTR  
     
          
SUBTR_NUM2:
    MOV BX,0
    POP AX   
    MOV AH,0
    SUB AL,30H
    ADD BX,AX
    MOV BX,AX
    DEC CX
    CMP CX,0
    JLE PRINT_RES_SUB
    POP AX
    MOV AH,0
    SUB AL,30H
    MOV DX,0AH
    MUL DX
    ADD AX,BX
    MOV BX,AX
    DEC CX
    CMP CX,0
    JLE PRINT_RES_SUB
    POP AX
    MOV AH,0
    SUB AL,30H
    MOV DX,64H
    MUL DX
    ADD AX,BX
    MOV BX,AX
    DEC CX
    JMP PRINT_RES_SUB               

            
            
            
    ;typwma diaforas 2arithmwn
PRINT_RES_SUB:
    MOV CX,AX
    POP BX
    MOV DX,BX  
    CMP BX,AX                   ;sigkrinw tous 2arithmous gia na vrw to megalitero
    JGE GR1S                    ;an o 1os>2os tote pigene sto GR1S opou tha kanei 1os-2os
    SUB CX,BX                   ;alliws kane 2os-1os 
    MOV BX,CX
    PUSH BX 
    MOV DX,2000                 ;2000 anagnwristikko gia arnitiko prosimo
    PUSH DX
    PRINT '='
    PRINT '-'
    JMP PRINT_HEX_SUB 
GR1S:                           ;1os>2os
    SUB BX,CX                   ;1os-2os
    MOV DX,BX
    PUSH DX                     ;vale to apotelesma sti stoiva 2fores afou sti synexeia
    PUSH BX                      ;tha prepei na vgalw to prosimo kai to apotelesma
    PRINT '=' 
PRINT_HEX_SUB:                 ;omoiws me pio panw
    MOV CX,4
    ROL BX,1
    ROL BX,1
    ROL BX,1
    ROL BX,1
    MOV DX,BX
    AND DX,000FH
    CMP DX,0
    JE PS3 
    JMP HEX_PS
    
PS3:
    MOV CX,3
    ROL BX,1
    ROL BX,1
    ROL BX,1
    ROL BX,1
    MOV DX,BX
    AND DX,000FH
    CMP DX,0
    JE PS2 
    JMP HEX_PS
    
PS2:
    MOV CX,2
    ROL BX,1
    ROL BX,1
    ROL BX,1
    ROL BX,1
    MOV DX,BX
    AND DX,000FH
    CMP DX,0
    JE PS1 
    JMP HEX_PS
    
PS1:
    MOV CX,1
    JMP HEX_PRIS    
        
    
HEX_PS:
    ROR BX,1
    ROR BX,1
    ROR BX,1
    ROR BX,1
HEX_PRIS:
    ROL BX,1
    ROL BX,1
    ROL BX,1
    ROL BX,1
    MOV DX,BX
    AND DX,000FH
    CALL PRINT_HEX
    LOOP HEX_PRIS
    
      
          
          
          
          
    PRINT '='
    POP DX              ;an DX=2000 anagnwristiko gia na typwsei -
    CMP DX,2000
    JE PLLL
    JMP TELLL
PLLL:
    PRINT '-'
TELLL:                ;typoma dekadikou
    POP BX
    MOV AX,BX          
    MOV CX,0
ADDR4:
    MOV DX,0
    MOV BX,10
    DIV BX
    PUSH DX
    INC CX
    CMP AX,0
    JNZ ADDR4
    
ADDR5:
    POP DX
    ADD DX,30H
    PRINT DL
    LOOP ADDR5
    JMP TELOS  
      
      

      
      
TELOS:      
    PRINT_STR KENI_GRAMMI       ;typwma kenis grammis
    JMP START                    ;sinexis leitourgia
    MOV AX,4C00H
    INT 21H
MAIN ENDP  
    
                 
                 
                 
PRINT_HEX PROC NEAR               ;diadikasia gia PRINT_HEX
    CMP DL,9
    JLE L9                         ;an o arithmos>9 prosthese 30H('0'=30H)
    ADD DL,37H                     ;alliws einai metaxi tou A kai tou F
    JMP PR                         ;ara prosthese 37('A'=41H')
L9:
    ADD DL,30H
PR:
    PRINT DL
    RET
PRINT_HEX ENDP                     