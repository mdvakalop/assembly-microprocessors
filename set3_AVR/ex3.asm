print_str macro string
    mov dx,offset string
    mov ah,9
    int 21h             
endm

print macro char
    mov dl,char
    mov ah,2
    int 21h
endm

read macro 
    mov ah,8
    int 21h
endm




data segment
    new_line db 0ah,0dh,'$'
    message db "Give 14 numbers or letters:",0ah,0dh,'$'
    store_addr dw 0810h
ends

stack segment
    dw   128  dup(0)
ends

code segment
    assume  cs::code, ss:stack, ds::data
    
start:
    mov ax,data
    mov ds,ax
    mov cx,14d 
    print_str message 
    
    mov si,0810h    ;store_addr 
    mov di,0820h    ;nums_addr
begin:
    read
    cmp al,'='      ;end of program
    je stop
    cmp al,0dh      ;if enter, sort
    je sort
    cmp al,20h      ;if space, print space
    jne not_space
    print al
    loop begin
    cmp cx,00h
    jle check_inp_15
not_space:
    cmp al,'0'
    jl begin        ;ignore
    cmp al,'9'
    jg letter       ;if bigger then check for letter
    mov [si],al     ;store num
    inc si
    print al
    loop begin
    cmp cx,00h
    jle check_inp_15 
letter:
    cmp al,'A'
    jl begin        ;ignore
    cmp al,'Z'
    jg little
    mov [si],al
    inc si
    print al
    loop begin 
    cmp cx,00h
    jle check_inp_15
little:
    cmp al,'a'
    jl begin        ;ignore
    cmp al,'z'
    jg begin        ;ingore
    mov [si],al     ;store little letter at [dx]
    inc si
    print al
    loop begin
    cmp cx,00h
    jle check_inp_15
    
check_inp_15:
    read            ;check if the 15th in is enter
    cmp al,'='      ;end of program
    je stop
    cmp al,0dh      ;if enter, sort 
    je sort
    print_str new_line
    jmp start
    
sort:
    print_str new_line
    mov cx,si       ;cx=last char +1

nums:
    mov si,080fh    ;store_addr -1
loop1:
    inc si
    cmp si,cx
    je caps
    mov al,[si]
    cmp al,'9'
    jg loop1         ;not a num,ignore
    mov [di],al     ;store num in num list
    inc di
    print al        ;for sure a num
    jmp loop1
    
caps:
    print 20h
    mov si,080fh    ;store_addr -1
loop2:
    inc si
    cmp si,cx
    je littles
    mov al,[si]
    cmp al,'A'
    jl loop2
    cmp al,'Z'
    jg loop2
    print al
    jmp loop2  
    
littles:
    print 20h
    mov si,080fh    ;store_addr -1
loop3:
    inc si
    cmp si,cx
    je max_nums
    mov al,[si]
    cmp al,'a'
    jl loop3
    cmp al,'z'
    jg loop3
    print al
    jmp loop3 
    
max_nums:
    print_str new_line 
    mov si,0820h    ;num_addr
    cmp si,di
    je pre_start
    
    mov bl,[si]     ;1st at in bl, 2nd in bh
    inc si
    cmp si,di
    je pre_start
    
    mov bh,[si]
    cmp bh,bl
    jg bh_bigger
    mov ah,bl       
    jmp cont:
bh_bigger:
    mov ah,bh        ;max in ah 
cont:
    inc si
    cmp si,di
    je done
    mov al,[si]
    cmp al,ah
    jg new_max      ;new max in al
    jl bigger_of_other
new_max:
    mov bl,ah       ;prev max in 1st, new max in 2nd 
    mov bh,al
    mov ah,al       ;new_ma in ah
    jmp cont
bigger_of_other:
    cmp al,bh
    jg flag1        ;if al>bh but al<bl (=ah=max)    
    cmp al,bl       ;check if al>bl but al<bh(=ah=max)
    jle cont        ;if al< both bh,bl, cont
    mov bl,bh       
    mov bh,al
    jmp cont
flag1:
    mov bh,al       ;just place the value in bh (al>bh)
    jmp cont

done:
    print bl
    print bh        
    
pre_start:
    print_str new_line
    jmp start  

stop:
mov ax, 4c00h
int 21h  

ends

end start
