INCLUDE MMM.TXT

DATA_SEG SEGMENT
    MSG1 DB "GIVE 2 DECIMAL DIGITS:",'$'
    MSG_SINEX DB 0AH,0DH,"GIVE 2 DECIMAL DIGITS:",'$'    
    MSG2 DB "OCTAL=",'$'    
    KENI_GRAMMI DB "      ",0AH,0DH,'$'
DATA_SEG ENDS

CODE_SEG SEGMENT
    ASSUME CS:CODE_SEG, DS:DATA_SEG SS:STACK_SEG
                                      
                                           
MAIN PROC FAR
    MOV AX,DATA_SEG
    MOV DS,AX
    PRINT_STR MSG1              ;prwto tipoma minimatos   
    JMP NUM1
SINEX_LEITOURGIA:
    PRINT_STR MSG_SINEX         ;typoma minimatos(afou einai sinexwmeni leitourgia
                                ;tha prepi na menei keni grammi apo tin proigumeni)                                
NUM1:
    READ                        ;kalw tin read 2fores gia tous prwtous
    CMP AL,'Q'                  ;2arithmous.An Q pigene sto telos
    JE TELOS                    ;an den einai eggyro psifio to 1o
    CMP AL,30H                  ;h to 2o perimene 2 eggira.
    JL NUM1
    CMP AL,39H
    JG NUM1     
    PUSH AX                     ;vazoume sti stiva to 1eggyro
    READ
    CMP AL,'Q'
    JE TELOS
    CMP AL,30H
    JL NUM1
    CMP AL,39H
    JG NUM1 
    MOV BL,AL                   ;filame to 2o eggiro ston register BL
NEXT_NUM:    
    READ                       ;kaloume tin DEC_KEYB_GR3 gia na diavasw
    CMP AL,'Q'                 ;ki allo arithmo h ENTER
    JE TELOS
    CMP AL,0DH                ;an dothi ENTER tote typose ta psifia k sinexise
    JE PRINT_DEC
    CMP AL,30H                 ;an den dothi eggiro psifio, tote prepei na
    JL NUM1                    ;perimenw 2eggira sti sinexia, opote
    CMP AL,39H                 ;paw se arxiki katastasi (NUM1)
    JG NUM1                    
    MOV BH,AL                  ;gia na ftasw edw, simenei pws pira kai 3o eggiro
    POP AX                     ;psifio. opote thelw ta 2teleftea. vgazw apo ti stoiva
    MOV AL,BL                  ;to 1o psifio, k vazw to 2. Etsi tha exw sti stiva
    PUSH AX                    ;to 2o psifio apo to telos k ston kataxwriti to telefteo
    MOV BL,BH
    JMP NEXT_NUM               ;pairnw sinexws numera mexri na patithei to ENTER
          
          
PRINT_DEC:                     ;tha typwsei ta 2teleftea dekadika psifia pou dextike                  
    POP AX                     ;sti stoiva exw to MSB(dekades) ara to vgazw gia na to typwsw.                             
    PRINT AL
    MOV BH,AL                  ;vazw to MSB ston BH
    MOV AL,BL                  ;to LSB (monades) vrisketai ston kataxwriti BL
    PRINT AL
    JMP CONV_BIN
    
CONV_BIN:
    PRINT_STR KENI_GRAMMI       ;allazw grammi
    PRINT_STR MSG2              ;typwnw MSG2
    MOV AL,BH                   ;BH dekades
    SUB AL,30H                  ;metatrepw se diadiko tis dekades
    MOV DL,0AH                  ;vazw sto DL to 10 wste na pollaplasiasti me AL pou einai dekades       
    MUL DL                      ;AL=AL*DL=AL*10
    MOV DL,AL                   ;DL=AL
    MOV AL,BL                   ;vazw ston AL tis monades
    SUB AL,30H                  ;metatrepw tis monades se diadiko
    ADD DL,AL                   ;AL=DL(dekades*10)+monades
                                ;filame to apotelesma to DL
    MOV DH,0                    ;DH=0 DL=apotelesma => DX=apotelesma
    
    
CONV_OCT:
    MOV CX,0                    ;metritis gia oktades
    MOV AX,DX                   ;apotelesma ston AX
ST:
    MOV DX,0                    ;DX=0
    MOV BX,8                    ;vazw sto BX to 8(oktades)
    DIV BX                      ;diairw to dekadiko(AX) me 8 kai to ypoloipo mpainei sti stoiva (DX ypol,AX phl)
    PUSH DX                     ;enw to piliko xrisimopoieitai gia tin epomeni diairesi 
    INC CX                      ;afxanw ton metriti psifiwn
    CMP AX,0                    ;elegxw an to piliko einai 0
    JNE ST                      ;an oxi sinexizw me diaireseis
    

ST_PRINT:
    POP DX                      ;bgazw ena-ena ta ypoloipa apo ti stoiva 
                                ;afto pou mpike telefteo einai to MSBoctal
    ADD DX,30H                  ;to metatrepw se ASCII wste na to emfanisw stin othoni
    PRINT DL                    ;to typwnw
    LOOP ST_PRINT               ;kanw CX tipomata osa k oi diaireseis poy eginan
          
    JMP SINEX_LEITOURGIA        ;sinexomeni leitourgia

TELOS:
    MOV AX,4C00H
    INT 21H
MAIN ENDP                
           
           
           
CODE_SEG ENDS
    END MAIN