	#include<m16def.inc>
	.def count=r16
	.def leds=r23
	.def reg=r17	
	.org 0x0
	rjmp reset
	.org 0x2
	rjmp ISR0
	.org 0x4
	rjmp ISR1

wait_usec:
sbiw r24 ,1 ; 2 circles (0.250 usec)
nop ; 1 circle (0.125 usec)
nop ; 1 circle (0.125 usec)
nop ; 1 circle (0.125 usec)
nop ; 1 circle (0.125 usec)
brne wait_usec ; 1 or 2 circles (0.125 or 0.250 usec)
ret ; 4 circles (0.500 usec

wait_msec:
push r24 ; 2 circles (0.250 usec)
push r25 ; 2 circles
ldi r24 , low(998) ; load r25:r24 with 998 (1 circle - 0.125 usec)
ldi r25 , high(998) ; 1 circle (0.125 usec)
rcall wait_usec ; 3 circles (0.375 usec), total delay 998.375 usec
pop r25 ; 2 circles (0.250 usec)
pop r24 ; 2 circles
sbiw r24 , 1 ; 2 circles
brne wait_msec ; 1 or 2 circles (0.125 or 0.250 usec)
ret ; 4 circles (0.500 usec)






reset:
	ldi reg,LOW(RAMEND)
	out SPL,reg
	ldi reg,HIGH(RAMEND)
	out SPH,reg


	ldi r24 ,( 1 << ISC11) | ( 1 << ISC10);fortonume sto mcucr na prokalite stiin Int1 me thetiki akmi i diakopi
	
	out MCUCR , r24 	
	ldi r24 ,( 1 << INT1) 	; energopiiise tin diakopi int1 sto gicr
	out GICR , r24
	sei			;energopiisi diakopon

	clr reg			;thelo pd7 san isodo ara kano to portD isodo 
	out DDRD,reg		;PORTD ->eisodos
	ser reg
	
	clr count		;count=0;
	ser r26 		
	out DDRA , r26		; arxikopiisi tis PORTA gia eksodo
	out DDRB , r26 		; arxikopiisi tis PORTB gia eksodo
	ldi leds,0		; arxikopiisi tu metritri diakopon
	out PORTA , leds	; dikse tin timi tu metriti diakopon sta leds eksodu thiras A
	ldi r26,0
loop:	out PORTB , r26 	; dikse timi t metritri sti thira eksodu B sta leds
			
	ldi r24 , low(200) 	; load r25:r24 with 200
	ldi r25 , high(200) 	; delay 200 ms
	rcall wait_msec
	inc r26 		; avksano metriti
	rjmp loop 		; epanelaba

ISR1:				;rutina eksipiretisis diakopis INT1
	push r26 		;filao tn kataxoriti (opoy einai o metritis mas tu kirios programmatos) sti stoiba
	in r26 , SREG 		; kai ton SREG=kataxoritis simaion
	push r26

loop1:
	out PORTA,leds		;mporei na min xriazete afti i entoli@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	ldi r24 ,(1 << INTF1)	;"midenise" me ti simasia tis ekfonisis to bit7 tu GIFR
	out GIFR ,r24 
	ldi r24 , low(5) 	;fortose tus r24-25 me 5
	ldi r25 , high(5) 	; delay 5msec
	rcall wait_msec
	in r24,GIFR  		;diabazume tn kataxoriti simeon diakopon GIFR gia na dume an egine t bit 7=0
	sbrc r24,7		;an bit7 tu GIFR einai miden tote kane skip tn epomeni entoli.
	rjmp loop1		;alios an den exei gini miden kai ara den stamatisan oi anapidisis,perimene mexri na stamatisun kai na gini 0
				
	in reg,PIND		;diabazo to periexomeno ts isodu D
	andi reg,128		;and reg me 1000 0000 =128 dec g na apomonoso t 7 bit diladi to PD7	
	cpi reg,128		;if reg=128dec=1000 0000 dld bit7 =1 
	brne noCount		;an den einai isa pigene sto noCount kai dikse  sta leds ti proigumeni metrisi (diladi oses diakopes exun idi ginei ,me PD7=1)
	inc count		;alios avksise to metriti ton diakopon
	mov leds,count		;metafere sta leds to metriti diakopon 
noCount:	
	out PORTA,leds		;tipose sta leds ti metrisi ton diakopon 
	pop r26
	out SREG , r26		; bgale tus kataxorites  r26, SREG apo ti stoiba oste na epanelthun oi sostes tus times gia to kirios programma
	pop r26
	reti			;epistrofi apo diakopi sto kirios programma
	
	ISR0:
