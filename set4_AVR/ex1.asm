.include "m16def.inc"
.def reg = r16
.def count = r17
.def A = r18
.cseg
.org 0

	ldi reg,LOW(RAMEND)
	out SPL,reg
	ldi reg,HIGH(RAMEND)
	out SPH,reg
	clr reg
	out DDRA,reg 	;portA input (A0)
	ser reg
	out DDRB,reg	;portB output
	out PORTB,reg 	;activate pull-ups of B

main:
	in A,PINA
	andi A,0b00000001	;check if A0 activated
	cpi A,0b00000001
	brne main		;if no, then stop and wait until A0=1 to continue
	rjmp left	

left_not_first_time:
	ldi count,2
	ldi reg,0b00000010
	rjmp to_left

left:
	ldi count,1	;count=1: counter to know when i have to.
	change direction(count=0 or 9)
	ldi reg,0b00000001	;LSB led	

to_left:
	ldi r24,low(500)
	ldi r25,high(500)	;r24:r25 for delay=0.5sec=500msec
	out PORTB,reg		;output B
	rcall wait_msec 	;call routine for delay
	
	
wait1:
	in A,pinA
	andi A,0b00000001	;check if A0 activated
	cpi A,0b00000001
	brne wait1		;if no, then stop and wait until A0=1 to continue
	lsl reg		;shift left for next position
	inc count	;count++
	cpi count,9	;if count==9 change direction to the right
	breq right
	rjmp to_left	;else continue moving to the left
	
		
right:
	ldi count,7
	ldi reg,0b01000000
	
to_right:
	ldi r24,low(500)
	ldi r25,high(500)	;r24:r25 for delay=0.5secs
	out PORTB,reg		;output B
	rcall wait_msec		;call routine for delay

wait2:
	in A,pinA
	andi A,0b00000001	;check if A0 activated
	cpi A,0b00000001	
	brne wait2		;if no, then stop and wait until A0=1 to continue
	lsr reg			;shift right for next position
	dec count		;count--
	cpi count,0		;if count==0 change direction to the left
	breq left_not_first_time
	rjmp to_right		;else continue moving to the right
	ret
	
	


wait_usec:
	sbiw r24,1 ;2 circles (0.250 usec)
	nop 	;1 circle (0.125 usec)
	nop 	;1 circle (0.125 usec)
	nop 	;1 circle (0.125 usec)
	nop 	;1 circle (0.125 usec)
	brne wait_usec ;1 or 2 circles (0.125 or 0.250 usec)
	ret 	;4 circles (0.500 usec)

wait_msec:
	push r24 	;2 circles (0.250 usec)
	push r25 	;2 circles
	ldi r24,low(998) 	;load r25:r24 with 998 (1 circle - 0.125 usec)
	ldi r25,high(998) 	;1 circle (0.125 usec)
	rcall wait_usec 	;3 circles (0.375 usec), total delay 998.375 usec
	pop r25 	;2 circles (0.250 usec)
	pop r24 	;2 circles
	sbiw r24,1 	;2 circles
	brne wait_msec 	;1 or 2 circles (0.125 or 0.250 usec)
	ret 		;4 circles (0.500 usec)
