#include <stdio.h>
#include <avr/io.h>

int ROL(int a){
	if (a==128) return 1;
	return a*2;
}

int ROR(int a){
	if (a==1)return 128;
	return a/2;
}

void main(void){
	int sw4,sw3,sw2,sw1,sw0,temp,t,leds;
	DDRA=0xff; //arxikopiisi thiras A os eksodo
	//PORTC=0xff;
	DDRC=0x00; //arxikopisii thiras C os eisodo
	PORTA=128;
	leds=128;
	while(1){
		temp=PINC;
		sw4=temp & 16;
		sw3=temp & 8;
		sw2=temp & 4;
		sw1=temp & 2;
		sw0=temp & 1;
		//synexis leitoyrgia
		if (sw4>0){
			while(sw4>0){
				sw4=PINC & 16;
			}
			leds=128;//anabei mono to telefteo led
		}
		else if (sw3>0){
			while(sw3>0){
				sw3=PINC & 8;
			}//peristrofi dio theseis aristera
			leds=ROL(leds);
			leds=ROL(leds);
		}
		else if (sw2>0){
			while(sw2>0){
				sw2=PINC & 4;
			}//peristrofi dio theseis deksia
			leds=ROR(leds);
			leds=ROR(leds);
		}
		else if (sw1>0) {
			while(sw1>0){
				sw1=PINC & 2;
			}//peristrofi mia thesi aristera
			eds=ROL(leds);
		}
		else if(sw0>0) {
			while(sw0>0){
				sw0=PINC & 1;
			}//peristrofi mia thesi deksia
			leds=ROR(leds);
		};
		PORTA=leds;
	};
}
