.include "m16def.inc"

	ldi r24,low(RAMEND)
	out SPL,r24
	ldi r24,high(RAMEND)
	out SPH,r24
	clr r26
	out DDRA,r26
	ser r26
	out DDRB,r26

flash:
	in r16,PINA 	;read number
	mov r17,r16
	andi r16,0b00001111 ;keep 4LSb
	andi r17,0b11110000 ;keep 4MSB
	lsr r17 	;4 digits shift right 
	lsr r17
	lsr r17
	lsr r17
	inc r16 	;increase because of the function (x+1)
	inc r17
	ldi r18,0xC8 	;move 200ms to register
	rcall on
	mul r16,r18 	;200ms mult x+1
	movw r24,r0 	;result in r0-r1
	rcall wait_msec
	rcall off
	mul r17,r18 	;same procedure as before
	movw r24,r0
	rcall wait_msec

rjmp flashon:
	ser r26
	out PORTB,r26
	ret

off:
	clr r26
	out PORTB,r26
	ret



wait_msec:
	push r24
	push r25
	ldi r24,low(998)
	ldi r25,high(998)
	rcall wait_usec
	pop r25
	pop r24
	sbiw r24,1
	brne wait_msec
	ret


wait_usec:
	sbiw r24,1
	nop
	nop
	nop
	nop
	brne wait_usec
	ret
