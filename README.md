Assembly 8085, 8086 and AVR exercises and small programs. <br/>
All AVR codes were tested on ATMEGA16 microcontroller. <br/>

Created during Microprocessors Laboratory course at NTUA ECE. <br/>
Developed by [Myrsini Vakalopoulou ](https://gitlab.com/mdvakalop) and [Georgios Hadjiharalambous](https://gitlab.com/Georgios.Hadjiharalambous)