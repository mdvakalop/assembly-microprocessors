#include <avr/io.h>
#include <stdio.h>

void main(void)
{
    int A, B, C, D, E, temp, f0, f1, f2, out1;
	DDRC = 0xff;	//PORTC = output
	DDRA = 0x00;	//PORTA = input
	PORTA = 0x00;
	PORTC = 0;
	while (1) {
		out1=0;
		temp = PINA;
		
		A = temp & 1;
		if (A>0) A=1; 
		B = temp & 2; 
		if (B>0) B=1;
		C = temp & 4;
		if (C>0) C=1;
		D = temp & 8;
		if (D>0) D=1;
		E = temp & 16; //apomonono ta bitakia atsssss ola ta A,B,..0 'h 1
		if (E>0) E=1;
	
		f0 = (~((A&B&C) | (C&D) | (D&E))) & 0x01;
		f1 = ((A&B&C) | ((~D)&(~E))) & 0x01;
		f2 = f0 | f1;
		if (f0) out1 = out1 | 128; //128 = 1000 0000 bin
		else out1 = out1 & (~128); //midenise to 1o msb
		if (f1) out1 = out1 | 64; //64 = 0100 0000 bin
		else out1 = out1 & (~64); //midenise to 2o msb
		if (f2) out1 = out1 | 32; //32 = 0010 0000 bin
		else out1 = out1 & (~32); //midenise to msb
		PORTC = out1;
	}
}

