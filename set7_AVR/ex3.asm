.include "m16def.inc"

.def temp1=r22
.def temp=r21
.def reg = r16
.def cmd=r17;metritis dekadon lepton
.def cmm =r19;metritis monadon lepton
.def csd=r18 ;metritis dekadon secs
.def csm=r20 ;metritis monadon secs


ldi temp,LOW(RAMEND)
	out SPL,temp
	ldi temp,HIGH(RAMEND)
	out SPH,temp 
	ser temp
	out DDRD,temp
	 
	
	rcall lcd_init		;initialize input  
	ldi cmm,0
	ldi cmd,0
	ldi csd,0
	ldi csm,0
	rcall zero 		;tiponei sto lcd to 00min:00sec
Waiting:
	in reg,PINB
	andi reg,0x01		;apomonono to proto bit
	cpi reg,0x01
	brne Waiting		;an den patithike to PB0 anameno
	rjmp Begin_Count
waitingBegin:
	in reg,PINB
	mov temp,reg
	andi temp,0x80
	cpi temp,0x80
	breq STOP
	andi reg,0x01
	cpi reg,0x01
	brne waitingBegin
			
		

Begin_Count:
	in reg,PINB
	mov temp,reg
	andi reg,0x80		;elegxo gia to pliktro PB7
	cpi reg,0x80
	breq STOP
	andi temp,0x01
	cpi temp,0x0
	breq waitingBegin		;an ine to PB0=0 tha stamata i metrisi kai perimenume na gini 1 gia na sinexisi
	

	inc csm
	cpi csm,0x0A		;sigrino me to 10
	breq Add1DekadaS
B:
	cpi csd,0x06		;sigirni me to 6
	breq ADDmin
A:	
	rcall tipoma
	ldi temp1,0x0A
check:
	
	ldi r24,LOW(100)
	ldi r25,HIGH(100)	;kathisterisi enos sec
	rcall wait_msec
	in reg,PINB
	mov temp,reg
	andi reg,0x80		;elegxo gia to pliktro PB7
	cpi reg,0x80
	breq STOP
	andi temp,0x01
	cpi temp,0x0
	breq waitingBegin		;an ine to PB0=0 tha stamata i metrisi kai perimenume na gini 1 gia na sinexisi
	
	
	dec temp1
	cpi temp1,0
	brne check















	rjmp Begin_Count
	
Add1DekadaS:
	inc csd		;avksano kata ena tn aritumo ton dekadon
	ldi csm,0	;midenizo tn metriti monadon secs
	rjmp B

	
ADDmin :
	inc cmm			;avksano tn metriti lepton
	ldi csd,0		;fortono sto metriti defterolepton to miden
	
	cpi cmm,0x0A		;sigrino me to 10
	breq Add1DekadaM
C:		;an ine iso me 60 to midenizo
	rjmp A
Add1DekadaM:
	ldi cmm,0
	inc cmd	
	cpi cmd,0x06		;sigrino me to 6
	breq ZeroMin
D:
	rjmp C
ZeroMin: 
	ldi cmd,0
	rjmp D

STOP:	;lcd_init		;tipono 00min:00sec k perimeno na ksanapatihei k na arxisi i metrisi ap tn arxi
	ldi r24 ,0x01		; ���������� ��� ������
	rcall lcd_command
	ldi r24 ,low(1530)
	ldi r25 ,high(1530)
	rcall wait_usec

	rcall zero
	ldi csm,0
	ldi csd,0
	ldi cmm,0
	ldi cmd,0
	rjmp Waiting

	
tipoma :
	;rcall lcd_init
	ldi r24 ,0x01		
	rcall lcd_command
	ldi r24 ,low(1530)
	ldi r25 ,high(1530)
	rcall wait_usec

	ldi reg,0x30 ;diladi to 30
	mov r24,cmd
	add r24,reg	;prostheto 30 stn arithmo gia na ton kano se kodika Aschi katalilo g tipoma
	rcall lcd_data
	mov r24,cmm
	add r24,reg
	rcall lcd_data
	ldi r24,' ' 		;to keno
	rcall lcd_data
	ldi r24,'M'
	rcall lcd_data
	ldi r24,'I'
	rcall lcd_data
	ldi r24,'N'
	rcall lcd_data
	ldi r24,':'
	rcall lcd_data
	ldi r24,' '
	rcall lcd_data
	mov r24,csd
	add r24,reg
	rcall lcd_data
	mov r24,csm
	add r24,reg
	;rcall lcd_data
	rcall lcd_data
	ldi r24,' '
	rcall lcd_data
	ldi r24,'S'
	rcall lcd_data
	ldi r24,'E'
	rcall lcd_data
	ldi r24,'C'
	rcall lcd_data
	ret
	


zero:
	;rcall lcd_init
	ldi r24 ,0x01		; ���������� ��� ������
	rcall lcd_command
	ldi r24 ,low(1530)
	ldi r25 ,high(1530)
	rcall wait_usec

	ldi r24,'0'		;tiponi ta dio miden
	rcall lcd_data
	ldi r24,'0'
	rcall lcd_data
	ldi r24,' ' 		;to keno
	rcall lcd_data
	ldi r24,'M'
	rcall lcd_data
	ldi r24,'I'
	rcall lcd_data
	ldi r24,'N'
	rcall lcd_data
	ldi r24,':'
	rcall lcd_data
	ldi r24,' '
	rcall lcd_data
	ldi r24,'0'
	rcall lcd_data
	ldi r24,'0'
	rcall lcd_data
	ldi r24,' '
	rcall lcd_data
	ldi r24,'S'
	rcall lcd_data
	ldi r24,'E'
	rcall lcd_data
	ldi r24,'C'
	rcall lcd_data
	ldi r24 ,low(530)
	ldi r25 ,high(530)
	rcall wait_msec
	ret
	



































//=============================END OF PROGRAM====================================//

//================================ROUTINES=======================================//
//===============================================================================//
//===============================================================================//
//===============================================================================//
wait_msec:
	push r24
	push r25
	ldi r24,low(998)
	ldi r25,high(998)
	rcall wait_usec
	pop r25
	pop r24
	sbiw r24,1
	brne wait_msec
	ret
//===============================================================================//
wait_usec:
	sbiw r24,1
	nop
	nop
	nop
	nop
	brne wait_usec
	ret
//===============================================================================//

//===============================================================================//
write_2_nibbles:
push r24 ; st???e? ta 4 MSB
in r25 ,PIND ; d?a?????ta? ta 4 LSB ?a? ta ?a?ast?????�e
andi r25 ,0x0f ; ??a ?a �?? ?a??s??�e t?? ?p??a p??????�e?? ?at?stas?
andi r24 ,0xf0 ; ap?�??????ta? ta 4 MSB ?a?
add r24 ,r25 ; s??d?????ta? �e ta p???p?????ta 4 LSB
out PORTD ,r24 ; ?a? d????ta? st?? ???d?
 sbi PORTD ,PD3 ; d?�?????e?ta? pa?�?? Enable st?? a???d??t? PD3
cbi PORTD ,PD3 ; PD3=1 ?a? �et? PD3=0
pop r24 ; st???e? ta 4 LSB. ??a?t?ta? t? byte.
 swap r24 ; e?a???ss??ta? ta 4 MSB �e ta 4 LSB
andi r24 ,0xf0 ; p?? �e t?? se??? t??? ap?st?????ta?
add r24 ,r25
out PORTD ,r24
 sbi PORTD ,PD3 ; ???? pa?�?? Enable
cbi PORTD ,PD3
ret
//===============================================================================//
lcd_data:
 sbi PORTD ,PD2 ; ep????? t?? ?ata????t? ded?�???? (PD2=1)
rcall write_2_nibbles ; ap?st??? t?? byte
ldi r24 ,43 ; a?a�??? 43�sec �???? ?a ?????????e? ? ????
ldi r25 ,0 ; t?? ded?�???? ap? t?? e?e??t? t?? lcd
rcall wait_usec
ret
//===============================================================================//
lcd_command:
cbi PORTD ,PD2 ; ep????? t?? ?ata????t? e?t???? (PD2=1)
rcall write_2_nibbles ; ap?st??? t?? e?t???? ?a? a?a�??? 39�sec
 ldi r24 ,39 ; ??a t?? ????????s? t?? e?t??es?? t?? ap? t?? e?e??t? t?? lcd.
ldi r25 ,0 ; S??.: ?p?????? d?? e?t????, ?? clear display ?a? return home,
rcall wait_usec ; p?? apa?t??? s?�a?t??? �e?a??te?? ??????? d??st?�a.
ret
//===============================================================================//
lcd_init:
ldi r24 ,40 ; ?ta? ? e?e??t?? t?? lcd t??f?d?te?ta? �e
ldi r25 ,0 ; ?e?�a e?te?e? t?? d??? t?? a?????p???s?.
rcall wait_msec ; ??a�??? 40 msec �???? a?t? ?a ?????????e?.
ldi r24 ,0x30 ; e?t??? �et??as?? se 8 bit mode
out PORTD ,r24 ; epe?d? de? �p????�e ?a e?�aste ???a???
 sbi PORTD ,PD3 ; ??a t? d?a�??f?s? e?s?d?? t?? e?e??t?
cbi PORTD ,PD3 ; t?? ??????, ? e?t??? ap?st???eta? d?? f????
ldi r24 ,39
ldi r25 ,0 ; e?? ? e?e??t?? t?? ?????? ???s?eta? se 8-bit mode
rcall wait_usec ; de? ?a s?�?e? t?p?ta, a??? a? ? e?e??t?? ??e? d?a�??f?s?
 ; e?s?d?? 4 bit ?a �eta?e? se d?a�??f?s? 8 bit
 ldi r24 ,0x30
out PORTD ,r24
 sbi PORTD ,PD3
cbi PORTD ,PD3
ldi r24 ,39
ldi r25 ,0
rcall wait_usec
ldi r24 ,0x20 ; a??a?? se 4-bit mode
out PORTD ,r24
 sbi PORTD ,PD3
cbi PORTD ,PD3
ldi r24 ,39
ldi r25 ,0
rcall wait_usec
 ldi r24 ,0x28 ; ep????? ?a?a?t???? �e?????? 5x8 ?????d??
 rcall lcd_command ; ?a? e�f???s? d?? ??a��?? st?? ?????
ldi r24 ,0x0c ; e?e???p???s? t?? ??????, ap?????? t?? ???s??a
 rcall lcd_command
 ldi r24 ,0x01 ; ?a?a??s�?? t?? ??????
rcall lcd_command
ldi r24 ,low(1530)
ldi r25 ,high(1530)
rcall wait_usec
 ldi r24 ,0x06 ; e?e???p???s? a?t?�at?? a???s?? ?at? 1 t?? d?e????s??
rcall lcd_command ; p?? e??a? ap????e?�??? st?? �et??t? d?e????se?? ?a?
 ; ape?e???p???s? t?? ???s??s?? ????????? t?? ??????
ret
//===============================================================================//

