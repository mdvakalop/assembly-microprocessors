.include "m16def.inc"

.def st_inp = r23
.def temp = r22
.def minus = r16
.def inp = r17
.def ekat = r18
.def dekad = r19
.def monad = r20
.def mflag = r21

	ldi temp,LOW(RAMEND)
	out SPL,temp
	ldi temp,HIGH(RAMEND)
	out SPH,temp 

	clr temp
	out ddra, temp	;porta input
	ser temp 
	out ddrd, temp
	clr temp
	out portd, temp 
	rcall lcd_init ;molis ti vgazo ekso apo to loop k kano clean stin arxi 
					;tou loop, kani monimo clean (ithele kai wait!!!!)

start:
	;rcall lcd_init	
	ldi r24, 0x01        
	rcall lcd_command	;clean lcd
	ldi r24 ,low(1530)
	ldi r25 ,high(1530)
	rcall wait_usec
	
	clr mflag
	clr ekat
	clr dekad
	clr monad
	ldi temp, 0x30		;for ascii in print_lcd
	
	in inp, pina
	mov st_inp, inp	
	rcall print_lcd_binary 
	ldi r24, '='		;print_lcd '='
	rcall lcd_data
	
	cpi inp, 0		;check if zero so to print "0" immediately
	breq print_0
	cpi inp, 0xff
	breq print_0

	clr minus		;when minus = 0, numb is postive
	sbrs inp, 7		;check bit7. if set (negative) skip next command
	rjmp positive
	com inp
	ldi minus, 1	;negative num -> minus != 0

positive:
	rcall to_decimal	;input in register inp, result in regs ekat, dekad, monad
	rcall print_lcd_decimal
	rjmp same_num

print_0:
	mov r24, temp	;temp = 0x30 for ascii
	rcall lcd_data	;print_lcd '0'

same_num:				;if not new num, stay here displaying current num
	in inp, pina
	cp inp, st_inp
	breq same_num
	rjmp start


;ROUTINES

to_decimal:				;from binary-hex to decimal
	cpi inp, 0x64
	brlo dekades	;if inp<100, jump to dekades
	inc ekat
	subi inp, 0x64	
dekades:
	cpi inp, 10
	brlo monades	;when inp<10, jump to monades
	inc dekad
	subi inp, 10		
	rjmp dekades
monades:
	cpi inp, 0
	breq telos
	inc monad
	dec inp
	rjmp monades
telos:
	ret


print_lcd_decimal:
	push r24
	cpi minus, 0
	breq print_plus
print_minus:
	ldi r24,'-'		;print_lcd '-'
	rcall lcd_data
	rjmp print_ekat
print_plus:
	ldi r24,'+'		;print_lcd '+'
	rcall lcd_data
print_ekat:
	cpi ekat, 0
	breq print_dek
	ldi mflag, 1
	add ekat, temp	;temp = 0x30 for ascii
	mov r24, ekat	
	rcall lcd_data	;print_lcd ekat
print_dek:
	cpi mflag, 1	;if mflag is one, means that dekad should be printed in any case: 0 or not
	breq pp
	cpi dekad, 0	;if mflag != 1 (mfalg = 0) then if dekad=0 -> dont print dekad, else print it and mflag=1
	breq print_mon
pp:	
	ldi mflag, 1
	add dekad, temp	;temp = 0x30 for ascii
	mov r24, dekad	
	rcall lcd_data	;print_lcd dekad
print_mon:
	add monad, temp	;temp = 0x30 for ascii
	mov r24, monad	
	rcall lcd_data	;print_lcd monad in any case, monad!=0

	pop r24
	ret


print_lcd_binary:	;prints whats in inp dekad = inp, ekat = ascii, monad = counter			
	push r24
	mov dekad, inp
	ldi monad, 8
loop8times:
	cpi monad, 0	;if did this 8 times, ret
	breq ptelos
	mov ekat, dekad
	andi ekat, 0b10000000	;check msb
	cpi ekat, 0		;if 0, print 0
	breq p0
	ldi ekat, 0x31	; else print 1
	mov r24, ekat
	rcall lcd_data	;print 1
	rjmp cont
p0:
	ldi ekat, 0x30
	mov r24, ekat
	rcall lcd_data	;print 0
cont:	
	dec monad	;dec counter
	lsl dekad	;logical shift left to take the next msb
	rjmp loop8times
ptelos:
	clr ekat
	clr dekad
	clr monad
	pop r24
	ret



lcd_data:		;byte p?? �etad?deta? st?? ????? st?? r24 (e?s?d??)
sbi PORTD ,PD2	; ep????? t?? ?ata????t? ded?�???? (PD2=1)
rcall write_2_nibbles	; ap?st??? t?? byte
ldi r24 ,43		; a?a�??? 43�sec �???? ?a ?????????e? ? ????
ldi r25 ,0		; t?? ded?�???? ap? t?? e?e??t? t?? lcd
rcall wait_usec
ret

lcd_command:	;e?t??? st?? r24 (e?s?d??)
cbi PORTD ,PD2	; ep????? t?? ?ata????t? e?t???? (PD2=1)
rcall write_2_nibbles	; ap?st??? t?? e?t???? ?a? a?a�??? 39�sec
ldi r24 ,39		; ??a t?? ????????s? t?? e?t??es?? t?? ap? t?? e?e??t? t?? lcd.
ldi r25 ,0		; S??.: ?p?????? d?? e?t????, ?? clear display ?a? return home,
rcall wait_usec ; p?? apa?t??? s?�a?t??? �e?a??te?? ??????? d??st?�a.
ret


lcd_init:
ldi r24 ,40		; ?ta? ? e?e??t?? t?? lcd t??f?d?te?ta? �e
ldi r25 ,0		; ?e?�a e?te?e? t?? d??? t?? a?????p???s?.
rcall wait_msec ; ??a�??? 40 msec �???? a?t? ?a ?????????e?.
ldi r24 ,0x30	; e?t??? �et?�as?? se 8 bit mode
out PORTD ,r24	; epe?d? de? �p????�e ?a e?�aste �?�a???
sbi PORTD ,PD3	; ??a t? d?a�??f?s? e?s?d?? t?? e?e??t?
cbi PORTD ,PD3	; t?? ??????, ? e?t??? ap?st???eta? d?? f????
ldi r24 ,39
ldi r25 ,0		; e?? ? e?e??t?? t?? ?????? �??s?eta? se 8-bit mode
rcall wait_usec ; de? ?a s?��e? t?p?ta, a??? a? ? e?e??t?? ??e? d?a�??f?s?
				; e?s?d?? 4 bit ?a �eta�e? se d?a�??f?s? 8 bit
ldi r24 ,0x30
out PORTD ,r24
sbi PORTD ,PD3
cbi PORTD ,PD3
ldi r24 ,39
ldi r25 ,0
rcall wait_usec
ldi r24 ,0x20	; a??a?? se 4-bit mode
out PORTD ,r24
sbi PORTD ,PD3
cbi PORTD ,PD3
ldi r24 ,39
ldi r25 ,0
rcall wait_usec
ldi r24 ,0x28	; ep????? ?a?a?t???? �e?????? 5x8 ?????d??
rcall lcd_command ; ?a? e�f???s? d?? ??a��?? st?? ?????
ldi r24 ,0x0c	; e?e???p???s? t?? ??????, ap?????? t?? ???s??a
rcall lcd_command
ldi r24 ,0x01	;?a?a??s�?? t?? ??????
rcall lcd_command
ldi r24 ,low(1530)
ldi r25 ,high(1530)
rcall wait_usec
ldi r24 ,0x06	; e?e???p???s? a?t?�at?? a???s?? ?at? 1 t?? d?e????s??
rcall lcd_command	; p?? e??a? ap????e?�??? st?? �et??t? d?e????se?? ?a?
					; ape?e???p???s? t?? ???s??s?? ????????? t?? ??????
ret


write_2_nibbles:
push r24		; st???e? ta 4 MSB
in r25 ,PIND	; d?a�????ta? ta 4 LSB ?a? ta ?a?ast?????�e
andi r25 ,0x0f	; ??a ?a �?? ?a??s??�e t?? ?p??a p??????�e?? ?at?stas?
andi r24 ,0xf0	; ap?�??????ta? ta 4 MSB ?a?
add r24 ,r25	; s??d?????ta? �e ta p???p?????ta 4 LSB
out PORTD ,r24	; ?a? d????ta? st?? ???d?
sbi PORTD ,PD3	; d?�?????e?ta? pa?�?? Enable st?? a???d??t? PD3
cbi PORTD ,PD3	; PD3=1 ?a? �et? PD3=0
pop r24			; st???e? ta 4 LSB. ??a?t?ta? t? byte.
swap r24		; e?a???ss??ta? ta 4 MSB �e ta 4 LSB
andi r24 ,0xf0	; p?? �e t?? se??? t??? ap?st?????ta?
add r24 ,r25
out PORTD ,r24
sbi PORTD ,PD3	; ???? pa?�?? Enable
cbi PORTD ,PD3
ret


wait_msec:
push r24
push r25
ldi r24,low(998)
ldi r25,high(998)
rcall wait_usec
pop r25
pop r24
sbiw r24,1
brne wait_msec
ret

wait_usec:
sbiw r24,1
nop
nop
nop
nop
brne wait_usec
ret
